package com.training;

public class Main {
    public static void main(String[] args) throws LuggageLimitException {
        Traveller t1 = new Traveller("Shruti", 15.1);
        Traveller t2 = new Traveller("Bhavya", 18);
        CalculateAdditionalWeightCharges c1= new CalculateAdditionalWeightCharges(t1.getWeight());
        CalculateAdditionalWeightCharges c2= new CalculateAdditionalWeightCharges(t2.getWeight());
    }
}