package com.training;

public class Traveller {
    private final String name;
    private final double weight;

    Traveller(String name, double weight){
        this.name=name;
        this.weight=weight;
    }
    public String getName(){
       return this.name;
    }
    public double getWeight(){
        return this.weight;
    }
}
