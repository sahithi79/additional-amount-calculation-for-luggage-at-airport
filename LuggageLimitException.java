package com.training;

public class LuggageLimitException extends Exception{
    LuggageLimitException(){
        super("Charges Exempted as difference is negligible");
    }
}
