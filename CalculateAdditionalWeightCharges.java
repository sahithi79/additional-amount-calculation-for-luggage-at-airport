package com.training;

public class CalculateAdditionalWeightCharges {

    public CalculateAdditionalWeightCharges(double weight){
        double additionalWeight = weight - 15;
        double costPerKg=500;
        double additionalCharges;
        if(additionalWeight<0){
            System.out.println("No additional charges as weight is less than 15");
        }
        else if(additionalWeight>0 && additionalWeight<0.5) {
            try {
                throw new LuggageLimitException();
            } catch (LuggageLimitException e) {
                e.printStackTrace();
            }
        }
        else{
            additionalCharges=additionalWeight*costPerKg;
            System.out.println(additionalCharges);
        }
    }
}
